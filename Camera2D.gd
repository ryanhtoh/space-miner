extends Camera2D

var zoom_amt = 1.5
var max_zoom = 15 #1.5
var min_zoom = 0.2

func _ready():
	gbl.Cam = self

func _input(event):
	if event.is_pressed() and event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_UP && get_zoom().x > min_zoom:
			set_zoom(get_zoom()/zoom_amt)
		elif event.button_index == BUTTON_WHEEL_DOWN && get_zoom().x < max_zoom:
			set_zoom(get_zoom()*zoom_amt)
