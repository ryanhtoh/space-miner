extends Node2D

var pos = Vector2(0,0)
var timed = 0
var refresh_rate = 0.5
var cycle = 1
var thread

onready var temp_map = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var heat_cap = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var t_conduct = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var pipe = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var water1 = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var water2 = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var water1_dir = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var water2_dir = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var water1_flow = create_map(gbl.mapsize.x, gbl.mapsize.y)
onready var water2_flow = create_map(gbl.mapsize.x, gbl.mapsize.y)

# Called when the node enters the scene tree for the first time.
func _ready():
	gbl.Proc = self
	update() # Queue the CanvasItem for update.
	for i in range(gbl.mapsize.x):
		for j in range(gbl.mapsize.y):
			temp_map[i][j] = 100 + randf() * 700
			heat_cap[i][j] = 800
			t_conduct[i][j] = 2
			pipe[i][j] = 0
			water1[i][j] = 0
			water2[i][j] = 0
			water1_dir[i][j] = Vector3.ZERO
			water2_dir[i][j] = Vector3.ZERO
			water1_flow[i][j] = Vector3.ZERO
			water2_flow[i][j] = Vector3.ZERO

func create_map(w, h):
	var map = []
	for _x in range(w):
		var col = []
		col.resize(h)
		map.append(col)
	return map

###################################################################################################
## DRAW CYCLE

func _process(delta):
	update() # Queue the CanvasItem for update.

func _draw():
	match gbl.overlay:
		1: draw_array(temp_map, 900)
		2: draw_array(t_conduct, 5)
		3: draw_water(water2)
		4: draw_water(water1)

func draw_array(array, max_val):
	var view = getView()
	var x_min = max(view[0].x/gbl.gridsize.x-1, 0)
	var y_min = max(view[0].y/gbl.gridsize.y-1, 0)
	var x_max = min(view[1].x/gbl.gridsize.x+1, gbl.mapsize.x)
	var y_max = min(view[1].y/gbl.gridsize.y+1, gbl.mapsize.y)
	for x in range(x_min, x_max):
		for y in range(y_min, y_max):
			pos.x = x*gbl.gridsize.x
			pos.y = y*gbl.gridsize.y
			var val = array[x][y]
			var hue = max(0.75*(max_val-val)/max_val, 0)
			var color = Color.from_hsv(hue, 1, 1, 0.5)
			draw_rect(Rect2(pos, gbl.gridsize), color)
			
func draw_water(array):
	var view = getView()
	var x_min = max(view[0].x/gbl.gridsize.x-1, 0)
	var y_min = max(view[0].y/gbl.gridsize.y-1, 0)
	var x_max = min(view[1].x/gbl.gridsize.x+1, gbl.mapsize.x)
	var y_max = min(view[1].y/gbl.gridsize.y+1, gbl.mapsize.y)
	var liquid_size = gbl.gridsize - Vector2(6,6)
	for x in range(x_min, x_max):
		for y in range(y_min, y_max):
			pos.x = x*gbl.gridsize.x
			pos.y = y*gbl.gridsize.y
			var val = array[x][y]
			var color = Color(0, 0, 1, max(0.8*val, 0))
			draw_rect(Rect2(pos+Vector2(3,3), liquid_size), color)

func getView():
	# Get view rectangle
	var ctrans = get_canvas_transform()
	var min_pos = -ctrans.get_origin() / ctrans.get_scale()
	var view_size = get_viewport_rect().size / ctrans.get_scale()
	var max_pos = min_pos + view_size
	return [min_pos, max_pos]

###################################################################################################
## PHYSICS CYCLE

func _physics_process(delta):
	timed += delta
	if timed > refresh_rate:
		if thread: thread.wait_to_finish()
		thread = Thread.new()
		thread.start(self, "update_grid")
		timed -= refresh_rate

func update_grid(arg):
	var items = get_tree().get_nodes_in_group ("items")
	if items:
		get_tree().call_group("items","move_tween")
		for item in items:
			var overlaps = item.get_overlapping_areas()
			for overlap in overlaps:
				if overlap.is_in_group("pushers"):
					print("tested")
	# Cycle
	if cycle == 1:
		cycle = 2
		update_temp_pix(Vector2.ZERO, gbl.mapsize - Vector2.ONE, 1)
		update_liqd_pix(Vector2.ONE, gbl.mapsize, 1, water1)
		update_liqd_pix(Vector2.ONE, gbl.mapsize, 1, water2)
	elif cycle == 2:
		cycle = 1
		update_temp_pix(gbl.mapsize - Vector2.ONE, Vector2(2,2), -1)
		update_liqd_pix(gbl.mapsize - Vector2.ONE, Vector2(2,2), -1, water2)
		update_liqd_pix(gbl.mapsize - Vector2.ONE, Vector2(2,2), -1, water1)
	
	# Pump water
	var pumps = get_tree().get_nodes_in_group("pumps")
	if pumps:
		for node in pumps:
			var x = node.position.x/gbl.gridsize.x
			var y = node.position.y/gbl.gridsize.y
			if water2[x][y] == 0: water2[x][y] = 1
	var heaters = get_tree().get_nodes_in_group("heaters")
	if heaters:
		for node in heaters:
			var x = node.position.x/gbl.gridsize.x
			var y = node.position.y/gbl.gridsize.y
			temp_map[x][y] += 0.5

func update_liqd_pix(start, end, increment, array):
	var dirs = water1_dir
	var flow = water1_flow
	var alt_array = water2
	var alt_dirs = water2_dir
	var alt_flow = water2_flow
	if array == water2:
		dirs = water2_dir
		flow = water2_flow
		alt_array = water1
		alt_dirs = water1_dir
		alt_flow = water1_flow
	
	for i in range(start.x, end.x, increment):
		for j in range(start.y, end.y, increment):
			if dirs[i][j] == Vector3.ZERO:
				array[i][j] = 0
				continue
			if dirs[i][j].z == INF:
				continue
			var i2 = i - increment
			var j2 = j - increment
			var polarity = -increment
			if (dirs[i][j].z == 1 and alt_array == water2) or (dirs[i][j].z == -1 and alt_array == water1):
				if flow[i][j].z == 0: # just transferred layers
					if alt_array[i][j] == 0: # space free
						alt_array[i][j] = array[i][j]
						alt_flow[i][j] = Vector3(0, 0, dirs[i][j].z)
						array[i][j] = 0
					continue # space not free
				else: polarity = increment # not just transferred layers
			elif dirs[i][j].z != 0: continue # invalid z dir
			# normal
			if dirs[i][j].x == polarity and array[i2][j] == 0:
				array[i2][j] = array[i][j]
				flow[i2][j] = dirs[i][j]
				array[i][j] = 0
			elif dirs[i][j].y == polarity and array[i][j2] == 0:
				array[i][j2] = array[i][j]
				flow[i][j2] = dirs[i][j]
				array[i][j] = 0

func update_temp_pix(start, end, increment):
	for i in range(start.x, end.x, increment):
		for j in range(start.y, end.y, increment):
			var i2 = i+increment
			var j2 = j+increment
			var kx = min(t_conduct[i][j], t_conduct[i2][j])
			var ky = min(t_conduct[i][j], t_conduct[i][j2])
			var qx = ( temp_map[i][j] - temp_map[i2][j] ) * kx
			var qy = ( temp_map[i][j] - temp_map[i][j2] ) * ky
			temp_map[i][j] -= (qx+qy) / heat_cap[i][j]
			temp_map[i2][j] += qx / heat_cap[i2][j]
			temp_map[i][j2] += qy / heat_cap[i][j2]
