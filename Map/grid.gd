extends Node2D

var L1_path = "../L1"
var L2_path = "../L2"
var blocks_path = "../Blocks"
var bg_path = "../BG"
var pump = preload("res://pump.tscn")
var pusher = preload("res://pusher.tscn")

var build_locs = []
var destroy_timer = 0
var time_delta = 0
var mouse_vec
var last_mouse_vec
var laser
var hard_blocks = [0]
var mining_delay = 0.3

func _ready():
	gbl.Grid = self
	laser = get_node("../player/laser")

func _input(event):
	var block_size = 2
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and not gbl.mouse_on_UI:
		if gbl.selected_item: place(gbl.selected_item)
	if Input.is_mouse_button_pressed(BUTTON_RIGHT) and not gbl.mouse_on_UI:
		if not get_node(blocks_path).get_cellv(mouse_vec * block_size) in hard_blocks:
			destroy()
		if not laser.is_casting: laser.set_is_casting(true)
	elif laser.is_casting: laser.set_is_casting(false)
		
func _process(delta):
	var x = floor(get_global_mouse_position().x/gbl.gridsize.x)
	var y = floor(get_global_mouse_position().y/gbl.gridsize.y)
	mouse_vec = Vector2(x, y)
	time_delta = delta
	
	# Mining destroy
	if last_mouse_vec != mouse_vec:
		destroy_timer = 0
	if Input.is_mouse_button_pressed(BUTTON_RIGHT) and not gbl.mouse_on_UI:
		last_mouse_vec = mouse_vec
		destroy_timer += delta
		print(destroy_timer)
	else:
		destroy_timer = 0
	if destroy_timer > mining_delay:
		destroy_timer = 0
		destroy()

func destroy():
	print("Destroy")
	if gbl.overlay == 3 or gbl.overlay == 4:
		set_pipe(-1)
	else:
		set_tile(blocks_path, -1, gbl.Proc.t_conduct, 0)

func place(item):
	var loc = Vector2(mouse_vec.x * gbl.gridsize.x + 8, mouse_vec.y * gbl.gridsize.y + 8)
	match item:
		"Pump": build(pump, loc)
		"Pipe": set_pipe(0)
		"Pipe (Left Turn)": set_pipe(2)
		"Pipe (Right Turn)": set_pipe(1)
		"Vent": set_pipe(6)
		"Pipe Out": set_pipe(8)
		"Pipe In": set_pipe(7)
		"Rock": set_tile(blocks_path, 0, gbl.Proc.t_conduct, invGbl.t_conducts["Rock"])
		"Insulation": set_tile(blocks_path, 1, gbl.Proc.t_conduct, invGbl.t_conducts["Insulation"])
		"Pusher": build(pusher, loc)

func set_tile(node, tile_id, array, array_val):
	var width = 2
	if tile_id != -1 and get_node(node).get_cellv(mouse_vec * width) != -1: return false
	array[mouse_vec.x][mouse_vec.y] = array_val
	for x in range(mouse_vec.x*width, (mouse_vec.x+1)*width):
		for y in range(mouse_vec.y*width, (mouse_vec.y+1)*width):
			get_node(node).set_cell(x, y, tile_id)
			get_node(node).update_bitmask_area(Vector2(x, y))

func set_pipe(val):
	gbl.Proc.pipe[mouse_vec.x][mouse_vec.y] = val
	var rotvars = find_dir(gbl.rotation)
	if gbl.overlay == 4:
		get_node(L1_path).set_cellv(mouse_vec, val, rotvars[0], rotvars[1], rotvars[2])
		get_node(L1_path).update_bitmask_area(mouse_vec)
		set_dir(get_node(L1_path), gbl.Proc.water1_dir, mouse_vec.x, mouse_vec.y)
	elif gbl.overlay == 3:
		get_node(L2_path).set_cellv(mouse_vec, val, rotvars[0], rotvars[1], rotvars[2])
		get_node(L2_path).update_bitmask_area(mouse_vec)
		set_dir(get_node(L2_path), gbl.Proc.water2_dir, mouse_vec.x, mouse_vec.y)
	else: print("Must switch to a pipe overlay")

func build(type, loc):
	if not loc in build_locs:
		var node = type.instance()
		build_locs.append(loc)
		node.position = loc
		node.rotation = gbl.rotation
		gbl.Root.add_child(node)

func find_dir(angle):
	var flip_x = false
	var flip_y = false
	var transpose = false 
	if angle == 0:
		return [flip_x, flip_y, transpose]
	elif angle == PI/2:
		flip_x = true
		transpose = true
	elif angle == PI:
		flip_x = true
		flip_y = true
	elif angle == 3*PI/2:
		flip_y = true
		transpose = true 
	return [flip_x, flip_y, transpose]
	
func set_dir(tilemap, dir_array, x, y):
	var cell_type = tilemap.get_cell(x, y)
	if cell_type >= 0:
		if cell_type == 6: dir_array[x][y] = Vector3(0, 0, INF)
		elif cell_type == 7: dir_array[x][y] = Vector3.FORWARD
		elif cell_type == 8: dir_array[x][y] = Vector3.BACK
		else: dir_array[x][y] = Vector3.ZERO
		
		var flip_x = tilemap.is_cell_x_flipped(x, y)
		var flip_y = tilemap.is_cell_y_flipped(x, y)
		var transpose = tilemap.is_cell_transposed(x, y)
		
		if flip_x and flip_y: dir_array[x][y] += Vector3.LEFT
		elif flip_x and transpose: dir_array[x][y] += Vector3.UP
		elif flip_y and transpose: dir_array[x][y] += Vector3.DOWN
		else: dir_array[x][y] += Vector3.RIGHT
	else: dir_array[x][y] = Vector3.ZERO
	print(dir_array[x][y])
