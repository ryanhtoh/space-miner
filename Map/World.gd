extends WorldEnvironment

var noise
var noise2
var rng = RandomNumberGenerator.new()

var map_size = gbl.mapsize
var grass_cap = 0.01
var road_caps = Vector2(0.3, 0.05)
var time = 0
var fired = false

var item = preload("res://GroundItem.tscn")

func _ready():
	gbl.Root = self
	randomize()
	rng.randomize()
	
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 5
	noise.period = 78
	noise.persistence = 0.5
	
	noise2 = OpenSimplexNoise.new()
	noise2.seed = randi()
	noise2.octaves = 2
	noise2.period = 2 #4
	noise2.persistence = 0.5
	
	make_rock()
	#makeA1()
	makeA2(100)
	
func _physics_process(delta):
	time += delta
	if time >= 4:
		time = 0
	if time > 1:
		if not fired:
			var pos = Vector2((gbl.mapsize.x/2+0.5)*gbl.gridsize.x, 2.5*gbl.gridsize.y)
			if check(pos):
				var node = item.instance()
				node.position = pos
				node.z_index = 10
				add_child(node)
			fired = true
	elif time < 1:
		fired = false

func make_rock():
	var conduct = invGbl.t_conducts["Rock"]
	for x in map_size.x:
		var a = floor( (noise.get_noise_1d(x) + 1) / 16 * map_size.x )
		for y in range(2*a, 2*map_size.y):
			$Blocks.set_cell(2*x, y, 0)
			$Blocks.set_cell(2*x+1, y, 0)
			gbl.Proc.t_conduct[x][floor(y/2)] = conduct
	$Blocks.update_bitmask_region(Vector2(0.0, 0.0), 2*Vector2(map_size.x, map_size.y))

func makeA1():
	for x in map_size.x:
		var a = floor( (noise.get_noise_1d(x) + 1) / 16 * map_size.x )
		for y in range(a, map_size.y):
			var b = noise2.get_noise_2d(x,y)
			if b > 0.1:
				$BG.set_cell(2*x, 2*y, 2)
				$BG.set_cell(2*x+1, 2*y, 2)
				$BG.set_cell(2*x, 2*y+1, 2)
				$BG.set_cell(2*x+1, 2*y+1, 2)
	$BG.update_bitmask_region(Vector2(0.0, 0.0), 2*Vector2(map_size.x, map_size.y))

func makeA2(spawns):
	for spawn in range(spawns):
		var xloc = rng.randi_range(5, map_size.x - 6)
		var a = floor( (noise.get_noise_1d(xloc) + 1) / 16 * map_size.x )
		var yloc = rng.randi_range(a+5, map_size.y/2 - 6)
		for x in range(xloc-3, xloc+3):
			for y in range(yloc-sqrt(9-pow(x-xloc,2)), yloc+sqrt(9-pow(x-xloc,2))+1 ):
				var b = noise2.get_noise_2d(x,y)
				if b < 0.0:
					$BG.set_cell(2*x, 2*y, 2)
					$BG.set_cell(2*x+1, 2*y, 2)
					$BG.set_cell(2*x, 2*y+1, 2)
					$BG.set_cell(2*x+1, 2*y+1, 2)
	$BG.update_bitmask_region(Vector2(0.0, 0.0), 2*Vector2(map_size.x, map_size.y))

func check(point):
	var space_state = $Blocks.get_world_2d().direct_space_state
	if not space_state.intersect_point(point):
		print("Not intersect")
		return true
	else:
		print(" intersect")
		return false
