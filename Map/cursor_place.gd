extends Node2D

func _input(event):
	cursor_update(event)

func cursor_update(event):
	if gbl.selected_item:
		$Sprite.texture = gbl.selected_item_ref.texture
		$Sprite.modulate = Color(0, 1, 0, 0.8)
		var mouse_pos = get_global_mouse_position()
		var grid_pos = mouse_pos / gbl.gridsize
		grid_pos.x = floor(grid_pos.x) + 0.5
		grid_pos.y = floor(grid_pos.y) + 0.5
		position = grid_pos * gbl.gridsize
		rotation = gbl.rotation
	else:
		$Sprite.texture = null
