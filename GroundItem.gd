extends Area2D

var dir = Vector2.RIGHT
onready var tween = $Tween

# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group("items")

func move_tween():
	tween.interpolate_property(self, "position",
		position, position + dir * gbl.gridsize.x,
		0.4, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
