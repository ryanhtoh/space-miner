extends CanvasLayer

var lbl = ""

func _ready():
	gbl.UI = self
	print(OS.window_size)

func _process(delta):
	if gbl.mouse_on_UI == false:
		cursor_update()
	else:
		$cursor.get_node("lbl").text = ""

func cursor_update():
	var mouse_pos = get_node("cursor").get_global_mouse_position()
	var array_pos = gbl.Grid.get_global_mouse_position()/gbl.gridsize
	array_pos.x = floor(array_pos.x)
	array_pos.y = floor(array_pos.y)
	$cursor.position = mouse_pos + Vector2(15, 0)
	match gbl.overlay:
		0:
			lbl = ""
		1:
			lbl = "Temp (K) = " + str( gbl.Proc.temp_map[array_pos.x][array_pos.y])
		2:
			lbl = "Temp Conductivity = " + str( gbl.Proc.t_conduct[array_pos.x][array_pos.y])
		3:
			lbl = "Water (m3) = " + str( gbl.Proc.water2[array_pos.x][array_pos.y])
		4:
			lbl = "Water (m3) = " + str( gbl.Proc.water1[array_pos.x][array_pos.y])
	$cursor.get_node("lbl").text = lbl

func _on_Inventory_mouse_entered():
	gbl.mouse_on_UI = true
func _on_Toolbar_mouse_entered():
	gbl.mouse_on_UI = true
func _on_CharacterPanel_mouse_entered():
	gbl.mouse_on_UI = true

func _on_Inventory_mouse_exited():
	gbl.mouse_on_UI = false
func _on_Toolbar_mouse_exited():
	gbl.mouse_on_UI = false
func _on_CharacterPanel_mouse_exited():
	gbl.mouse_on_UI = false
