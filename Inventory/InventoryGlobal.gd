extends Node

enum SlotType {
	SLOT_DEFAULT = 0,
	SLOT_HELMET,
	SLOT_ARMOR,
	SLOT_FEET,
	SLOT_NECK,
	SLOT_RING,
	SLOT_RING2,
	SLOT_LHAND,
	SLOT_RHAND
}

enum ItemRarity {
	COMMON = 0,
	UNCOMMON,
	RARE,
	EPIC,
	LEGENDARY
}

const RarityColor = {
	ItemRarity.COMMON: {"background": "gray"},
	ItemRarity.UNCOMMON: {"background": "green"},
	ItemRarity.RARE: {"background": "blue"},
	ItemRarity.EPIC: {"background": "purple"},
	ItemRarity.LEGENDARY: {"background": "orange"}
}

var t_conducts = {
	"Rock": 3,
	"Insulation": 0,
}

var itemDictionary = {
	0: {
		"itemName": "Pump",
		"itemText": "Pumps water",
		"itemIcon": preload("res://graphics/pump32.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	1: {
		"itemName": "Pipe",
		"itemText": "Pipes water",
		"itemIcon": preload("res://graphics/pipe_icon.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	2: {
		"itemName": "Pusher",
		"itemText": "Pushes",
		"itemIcon": preload("res://graphics/pusher32.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	3: {
		"itemName": "Insulation",
		"itemText": "Insulates",
		"itemIcon": preload("res://graphics/Insulation_icon.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	4: {
		"itemName": "Pipe (Left Turn)",
		"itemText": "Pipes",
		"itemIcon": preload("res://graphics/pipeCCW.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	5: {
		"itemName": "Pipe (Right Turn)",
		"itemText": "Pipes",
		"itemIcon": preload("res://graphics/pipeCW.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	6: {
		"itemName": "Rock",
		"itemText": "Rock",
		"itemIcon": preload("res://graphics/Rock_icon.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	7: {
		"itemName": "Vent",
		"itemText": "Vent",
		"itemIcon": preload("res://graphics/vent.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	8: {
		"itemName": "Anorthite Smelter",
		"itemText": "Smelts CaAl2Si2O8",
		"itemIcon": preload("res://graphics/vent.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	9: {
		"itemName": "Pipe Out",
		"itemText": "Pipe Out",
		"itemIcon": preload("res://graphics/pipe_out.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	10: {
		"itemName": "Pipe In",
		"itemText": "Pipe In",
		"itemIcon": preload("res://graphics/pipe_in.png"),
		"slotType": SlotType.SLOT_DEFAULT,
	},
	#	3: {
#		"itemName": "Heat Gun",
#		"itemText": 12,
#		"itemIcon": preload("res://Inventory/images/C_Elm03.png"),
#		"slotType": SlotType.SLOT_DEFAULT,
#	},
#	3: {
#		"itemName": "Heat Source",
#		"itemText": 12,
#		"itemIcon": preload("res://Inventory/images/P_Red02.png"),
#		"slotType": SlotType.SLOT_DEFAULT,
#	},
}
