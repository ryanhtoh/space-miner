extends Panel

var slotType = invGbl.SlotType.SLOT_DEFAULT

var slotIndex
var item = null
var style
var selected = false

func _init():
	mouse_filter = Control.MOUSE_FILTER_PASS
	rect_min_size = Vector2(34, 34)
	style = StyleBoxFlat.new()
	refreshColors()
	set('custom_styles/panel', style)

func setItem(newItem):
	add_child(newItem)
	item = newItem
	item.itemSlot = self
	refreshColors()

func pickItem():
	item.pickItem()
	remove_child(item)
	gbl.UI.add_child(item)
	item = null
	refreshColors()

func putItem(newItem):
	item = newItem
	item.itemSlot = self
	item.putItem()
	gbl.UI.remove_child(item)
	add_child(item)
	refreshColors()

func removeItem():
	remove_child(item)
	item = null
	refreshColors()

func equipItem(newItem, rightClick =  true):
	item = newItem
	item.itemSlot = self
	item.putItem()
	if !rightClick:
		gbl.UI.remove_child(item)
	add_child(item)
	refreshColors()

func refreshColors():
	style.set_border_width_all(2)
	style.border_color = Color("#534434")
	if item:
		style.bg_color = ColorN(invGbl.RarityColor[item.rarity].background, 1)
	else:
		style.bg_color = Color("#8B7258")
	if selected:
		style.border_color = ColorN("yellow", 1)
		style.set_border_width_all(3)
