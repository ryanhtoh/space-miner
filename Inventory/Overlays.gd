extends Panel

func _ready():
	gbl.overlayUI = self

func select_overlay(num):
	disable_tabs()
	gbl.Root.get_node("L1").show()
	gbl.Root.get_node("L2").show()
	gbl.overlay = num
	match num:
		1: get_node("F1").pressed = true
		2: get_node("F2").pressed = true
		3:
			get_node("F3").pressed = true
			gbl.Root.get_node("L1").hide()
		4:
			get_node("F4").pressed = true
			gbl.Root.get_node("L2").hide()

func disable_tabs():
	get_node("F1").pressed = false
	get_node("F2").pressed = false
	get_node("F3").pressed = false
	get_node("F4").pressed = false

func _on_F1_toggled(button_pressed):
	pass

func _on_F2_toggled(button_pressed):
	pass

func _on_F3_toggled(button_pressed):
	pass

func _on_F4_toggled(button_pressed):
	pass
