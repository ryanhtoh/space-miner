extends TextureRect

var itemIcon
var itemName
var itemText
var itemSlot
var slotType
var picked = false
var rarity = 0

var rng = RandomNumberGenerator.new()

func _init(_itemName, _itemTexture, _itemSlot, _itemText, _slotType):
	rng.randomize()
	self.itemName = _itemName
	self.itemText = _itemText
	self.itemSlot = _itemSlot
	self.slotType = _slotType
	self.rarity = invGbl.ItemRarity.COMMON #rng.randi_range(invGbl.ItemRarity.COMMON, invGbl.ItemRarity.LEGENDARY)
	#rect_position = Vector2(2,2)
	texture = _itemTexture
	mouse_filter = Control.MOUSE_FILTER_PASS
	mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND

func pickItem():
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	picked = true

func putItem():
	rect_position = Vector2(0, 0)
	mouse_filter = Control.MOUSE_FILTER_PASS
	picked = false
