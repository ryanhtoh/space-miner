extends NinePatchRect

const ItemClass = preload("res://Inventory/Item.gd")

onready var itemNameLabel = get_node("Item Name")
onready var itemTextLabel = get_node("Text")
onready var size = Vector2(128, 64)

func get_quad(pos):
	var offset = Vector2(pos.x + 5, pos.y + 5)
	var win_ctr = OS.window_size/2
	if pos.x > win_ctr.x:
		offset.x = pos.x - size.x
	if pos.y > win_ctr.y:
		offset.y = pos.y - size.y
	return offset

func display(_item : ItemClass, mousePos : Vector2):
	visible = true
	itemNameLabel.set_text(_item.itemName)
	itemTextLabel.set_text(_item.itemText)
	rect_size = size
	rect_global_position = get_quad(mousePos)
