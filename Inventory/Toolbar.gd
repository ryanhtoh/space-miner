extends Panel


const ItemClass = preload("res://Inventory/Item.gd")
const ItemSlotClass = preload("res://Inventory/ItemSlot.gd")
const TooltipClass = preload("res://Inventory/Tooltip.gd")
const MAX_SLOTS = 10
var slotList = Array()

func _ready():
	gbl.Toolbar = self
	var slots = get_node("ToolSlots")
	for _i in range(MAX_SLOTS):
		var slot = ItemSlotClass.new()
		slotList.append(slot)
		slots.add_child(slot)

func select_slot(num):
	for i in range(MAX_SLOTS):
		slotList[i].selected = false
		slotList[i].refreshColors()
	if num > 0: # Select a new slot
		gbl.selected = num
		slotList[gbl.selected-1].selected = true
		slotList[gbl.selected-1].refreshColors()
