extends Panel

var slots = Array()

func _ready():
	slots.resize(512)
	slots.insert(invGbl.SlotType.SLOT_HELMET, get_node("Left/SlotHelmet"))
	slots.insert(invGbl.SlotType.SLOT_ARMOR, get_node("Left/SlotArmor"))
	slots.insert(invGbl.SlotType.SLOT_FEET, get_node("Left/SlotFeet"))
	slots.insert(invGbl.SlotType.SLOT_NECK, get_node("Left/SlotNeck"))
	
	slots.insert(invGbl.SlotType.SLOT_RING, get_node("Right/SlotRing"))
	slots.insert(invGbl.SlotType.SLOT_RING2, get_node("Right/SlotRing2"))
	slots.insert(invGbl.SlotType.SLOT_LHAND, get_node("Right/SlotLHand"))
	slots.insert(invGbl.SlotType.SLOT_RHAND, get_node("Right/SlotRHand"))

func getSlotByType(type):
	if type == invGbl.SlotType.SLOT_RING:
		return [slots[invGbl.SlotType.SLOT_RING], slots[invGbl.SlotType.SLOT_RING2]]
		
	return slots[type]

func getItemByType(type):
	return slots[type].item
