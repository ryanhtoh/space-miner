extends Panel

const ItemClass = preload("res://Inventory/Item.gd")
const ItemSlotClass = preload("res://Inventory/ItemSlot.gd")
const TooltipClass = preload("res://Inventory/Tooltip.gd")

const MAX_SLOTS = 45

var slotList = Array()

var holdingItem = null
var itemOffset = Vector2(0, 0)

onready var tooltip = get_node("../Tooltip")
onready var characterPanel = get_node("../CharacterPanel")
onready var toolbar = get_node("../Toolbar")

func _ready():
	var slots = get_node("SlotsContainer/Slots")
	for _i in range(MAX_SLOTS):
		var slot = ItemSlotClass.new()
		slot.connect("mouse_entered", self, "mouse_enter_slot", [slot])
		slot.connect("mouse_exited", self, "mouse_exit_slot", [slot])
		slot.connect("gui_input", self, "slot_gui_input", [slot])
		slotList.append(slot)
		slots.add_child(slot)

	for i in range(10):
		if i == 0:
			continue
		var panelSlot = characterPanel.slots[i]
		if panelSlot:
			panelSlot.connect("mouse_entered", self, "mouse_enter_slot", [panelSlot])
			panelSlot.connect("mouse_exited", self, "mouse_exit_slot", [panelSlot])
			panelSlot.connect("gui_input", self, "slot_gui_input", [panelSlot])
			
	for slot in toolbar.slotList:
		slot.connect("mouse_entered", self, "mouse_enter_slot", [slot])
		slot.connect("mouse_exited", self, "mouse_exit_slot", [slot])
		slot.connect("gui_input", self, "slot_gui_input", [slot])

func _process(delta):
	get_node("../FPS").text = "FPS: " + str( 1/(delta+.00001) ) + "\n Zoom: " + str( gbl.Cam.get_zoom().x )

func mouse_enter_slot(_slot : ItemSlotClass):
	gbl.mouse_on_UI = true
	if _slot.item:
		tooltip.display(_slot.item, get_global_mouse_position())

func mouse_exit_slot(_slot : ItemSlotClass):
	gbl.mouse_on_UI = false
	if tooltip.visible:
		tooltip.hide()

func slot_gui_input(event : InputEvent, slot : ItemSlotClass):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT && event.pressed:
			if holdingItem:
				if slot.slotType != invGbl.SlotType.SLOT_DEFAULT: # Equip held item
					if canEquip(holdingItem, slot):
						if !slot.item:
							slot.equipItem(holdingItem, false)
							holdingItem = null
						else:
							var tempItem = slot.item
							slot.pickItem()
							tempItem.rect_global_position = event.global_position - itemOffset
							slot.equipItem(holdingItem, false)
							holdingItem = tempItem
				elif slot.item: # Swap held item
					var tempItem = slot.item
					slot.pickItem()
					tempItem.rect_global_position = event.global_position - itemOffset
					slot.putItem(holdingItem)
					holdingItem = tempItem
					if slot == toolbar.slotList[gbl.selected-1] and gbl.selected != 0:
						gbl.selected_item = slot.item.itemName
						gbl.selected_item_ref = slot.item
				else: # Place held item
					slot.putItem(holdingItem)
					holdingItem = null
					if slot == toolbar.slotList[gbl.selected-1] and gbl.selected != 0:
						gbl.selected_item = slot.item.itemName
						gbl.selected_item_ref = slot.item
			elif slot.item: # Hold selected item
				holdingItem = slot.item
				itemOffset = event.global_position - holdingItem.rect_global_position
				slot.pickItem()
				holdingItem.rect_global_position = event.global_position - itemOffset
		elif event.button_index == BUTTON_RIGHT && !event.pressed:
			if slot.slotType != invGbl.SlotType.SLOT_DEFAULT: # Unequip item
				if slot.item:
					var freeSlot = getFreeSlot()
					if freeSlot:
						var item = slot.item
						slot.removeItem()
						freeSlot.setItem(item)
			else:
				if slot.item: # Equip Item
					var itemSlotType = slot.item.slotType
					var panelSlot = characterPanel.getSlotByType(slot.item.slotType)
					if itemSlotType == invGbl.SlotType.SLOT_RING: # Equip Ring
						if panelSlot[0].item && panelSlot[1].item:
							var panelItem = panelSlot[0].item
							panelSlot[0].removeItem()
							var slotItem = slot.item
							slot.removeItem()
							slot.setItem(panelItem)
							panelSlot[0].setItem(slotItem)
							pass
						elif !panelSlot[0].item && panelSlot[1].item || !panelSlot[0].item && !panelSlot[1].item:
							var tempItem = slot.item
							slot.removeItem()
							panelSlot[0].equipItem(tempItem)
						elif panelSlot[0].item && !panelSlot[1].item:
							var tempItem = slot.item
							slot.removeItem()
							panelSlot[1].equipItem(tempItem)
							pass
					else: # Equip Non-Ring
						if panelSlot.item:
							var panelItem = panelSlot.item
							panelSlot.removeItem()
							var slotItem = slot.item
							slot.removeItem()
							slot.setItem(panelItem)
							panelSlot.setItem(slotItem)
						else:
							var tempItem = slot.item
							slot.removeItem()
							panelSlot.equipItem(tempItem)

func _input(event : InputEvent):
	if holdingItem && holdingItem.picked and event is InputEventMouseMotion:
		holdingItem.rect_global_position = event.global_position - itemOffset

func getFreeSlot():
	for slot in slotList:
		if !slot.item:
			return slot

func canEquip(item, slot):
	var ring = invGbl.SlotType.SLOT_RING
	var ring2 = invGbl.SlotType.SLOT_RING2
	return item.slotType == slot.slotType || item.slotType == ring && (slot.slotType == ring || slot.slotType == ring2)

func _on_SortName_pressed():
	var items = Array()
	for slot in slotList:
		if slot.item:
			items.append(slot.item)
			slot.removeItem()
	items.sort_custom(self, "sortItemsByName")
	for i in range(items.size()):
		var item = items[i]
		var slot = slotList[i]
		slot.setItem(item)

func sortItemsByName(itemA : ItemClass, itemB : ItemClass):
	return itemA.itemName < itemB.itemName

func _on_AddItemButton_pressed():
	var slot = getFreeSlot()
	if slot:
		var item = invGbl.itemDictionary[randi() % invGbl.itemDictionary.size()]
		var itemName = item.itemName
		var itemIcon = item.itemIcon
		var itemText = item.itemText
		var slotType = item.slotType
		slot.setItem(ItemClass.new(itemName, itemIcon, null, itemText, slotType))

func pickup(type):
	var slot = getFreeSlot()
	if slot:
		var item = invGbl.itemDictionary[type]
		var itemName = item.itemName
		var itemIcon = item.itemIcon
		var itemText = item.itemText
		var slotType = item.slotType
		slot.setItem(ItemClass.new(itemName, itemIcon, null, itemText, slotType))
