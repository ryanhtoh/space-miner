extends Area2D

var fired = false

func _ready():
	add_to_group("pushers")

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_RIGHT:
		gbl.Grid.build_locs.erase(self.position)
		self.queue_free()
