extends KinematicBody2D

var velocity
export (int) var move = 200

func _ready():
	position = Vector2(gbl.mapsize.x*gbl.gridsize.x/2, 100)
	for idx in range(11):
		get_node("../UI/Inventory").pickup(idx)

func _physics_process(delta):
	get_input()
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.slide(collision.normal) 

func get_input():
	velocity = Vector2.ZERO
	if Input.is_action_pressed("W"):
		velocity += Vector2(0,-move)
	if Input.is_action_pressed("S"):
		velocity += Vector2(0,move)
	if Input.is_action_pressed("A"):
		velocity += Vector2(-move,0)
	if Input.is_action_pressed("D"):
		velocity += Vector2(move,0)
	if Input.is_action_pressed("pickup"):
		for body in $touch.get_overlapping_bodies():
			if body != self:
				body.queue_free()
				get_node("../UI/Inventory").pickup(1)
