extends Node2D

func _input(event):
	if event.is_pressed() and not event.is_echo():
		if Input.is_action_pressed("F1"):
			if gbl.overlay != 1: gbl.overlayUI.select_overlay(1)
			else: gbl.overlayUI.select_overlay(0)
		if Input.is_action_pressed("F2"):
			if gbl.overlay != 2: gbl.overlayUI.select_overlay(2)
			else: gbl.overlayUI.select_overlay(0)
		if Input.is_action_pressed("F3"):
			if gbl.overlay != 3: gbl.overlayUI.select_overlay(3)
			else: gbl.overlayUI.select_overlay(0)
		if Input.is_action_pressed("F4"):
			if gbl.overlay != 4: gbl.overlayUI.select_overlay(4)
			else: gbl.overlayUI.select_overlay(0)
		if Input.is_action_pressed("F5"):
			if gbl.overlay != 5: gbl.overlay = 5
			else: gbl.overlay = 0
		if Input.is_action_pressed("ui_left"): gbl.rotation = PI
		if Input.is_action_pressed("ui_right"): gbl.rotation = 0
		if Input.is_action_pressed("ui_up"): gbl.rotation = 3*PI/2
		if Input.is_action_pressed("ui_down"): gbl.rotation = PI/2
		if Input.is_action_pressed("R"): gbl.rotation += PI/2
		if gbl.rotation == 2*PI: gbl.rotation = 0
		
		if event is InputEventKey:
			var key_num
			match event.scancode:
				KEY_1: key_num = 1
				KEY_2: key_num = 2
				KEY_3: key_num = 3
				KEY_4: key_num = 4
				KEY_5: key_num = 5
				KEY_6: key_num = 6
				KEY_7: key_num = 7
				KEY_8: key_num = 8
				KEY_9: key_num = 9
				KEY_0: key_num = 10
			if key_num:
				if gbl.selected != key_num:
					gbl.selected = key_num
					gbl.Toolbar.select_slot(key_num)
					var item = gbl.Toolbar.slotList[gbl.selected-1].item
					if item:
						gbl.selected_item = item.itemName
						gbl.selected_item_ref = item
					else:
						gbl.selected_item = null
						gbl.selected_item_ref = null
					print(gbl.selected_item)
				else:
					gbl.Toolbar.select_slot(0)
					gbl.selected = 0
					gbl.selected_item = null
